import java.util.Scanner;
public class PokerShort {
  public static void main(String[] args) {
    
      boolean startGame;
      boolean gameOver = false;
      Scanner in = new Scanner(System.in);
      System.out.println("+--------------------+");
      System.out.println("Welcome to Java Poker \nby Vitaliy Aleksyuk");
      System.out.println("+--------------------+");
      int pot = 0;//pot variable, will have the bets deposited into it and then distributed to the winner 
      Deck playingDeck = new Deck();
      DynamicArray newHand1 = new DynamicArray();
      DynamicArray newHand2 = new DynamicArray();
      System.out.println();
      System.out.println("+------------------------+");
      System.out.println("The deck has been created");
      System.out.println("+------------------------+");

      //Creating the players object, using user input for names, 
      //a predetermined value for the chips and a hand object that was initialized earlier
      System.out.println("Player 1, what is your name?");
      System.out.println("+---------------------------+");
      String p1Name = in.nextLine();
      
        Player p1 = new Player(p1Name, 1000, newHand1);
        System.out.println("Welcome " + p1Name +" You have: "+ p1.getChips()+" chips");
        System.out.println("+--------------------------------+");
      System.out.println("Player 2, what is your name?");
      System.out.println("+---------------------------+");
      String p2Name = in.nextLine();

        Player p2 = new Player(p2Name, 1000, newHand2);
        System.out.println("Welcome " + p2Name +" You have: "+ p2.getChips()+" chips");
        System.out.println("+--------------------------------+");
      //Asking if the players are ready and if they want to play
    
      int currentBet  = 10;
      int hand1Index = 0;
      int hand2Index = 0;
      
      System.out.println("Are you ready?\nan entry fee of 10 chips is required\ny or n");
      System.out.println("+--------------------------------+");
      String ready = in.nextLine();
      
      if (ready.toLowerCase().charAt(0) == 'y' ) {
        startGame=true;
      }
      else {
        startGame=false;
      }
      if (startGame) {
        while (!gameOver){
      //take some chips from each player as entry fee
      
        System.out.println("+-----------------------------------------------------+");
        System.out.println("10 chips have been taken from each player as entry fee");
        System.out.println("+-----------------------------------------------------+");
        pot += p1.takeChips(currentBet);
        pot += p2.takeChips(currentBet);

      //Filling hands with the 3 initial cards
        for (int i = 0; i < 3; i++) {
          newHand1.insertCard(playingDeck.getRandomCard(), i);
          hand1Index++;
          newHand2.insertCard(playingDeck.getRandomCard(), i);
          hand2Index++;
        }
        System.out.println("The pot is currently holding: " + pot+ " chips");
        System.out.println("+-----------------------------------------------------+");
        while (hand2Index < 6) {
        //Showing hands for the first time
        String betChoise;
        System.out.println("This is "+p1Name+ "'s hand:");
        System.out.println("+---------------------------+");
        System.out.println(p1.printHand());
        System.out.println("+---------------------------+");
        //showing available option to the player
        System.out.println("--------  ---------  ---------  --------");
        System.out.println("| FOLD |  | CHECK |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
        System.out.println("--------  ---------  ---------  --------");
        //handles bad user input and exits the app without crashing
          try {
            System.out.println("input one of the actions above");
            betChoise = in.nextLine();
        //checking the user's input and doing the action they input
          switch (betChoise.toLowerCase()) {
            case "fold":
            System.out.println(p1Name+" Folds, "+p2Name+" Wins");
                p1.fold();
              break;
            case "check":
                p1.check();
              break;   
            case "raise":
            currentBet += p1.raise(currentBet);
                System.out.println(currentBet);
                pot += p2.takeChips(currentBet);
                  System.out.println("The pot is currently holding: " + pot+ " chips");
                  System.out.println("+-----------------------------------------------------+");
              break;
            case "call":
                p1.call(currentBet);
                pot += p2.takeChips(currentBet);
                  System.out.println("The pot is currently holding: " + pot+ " chips");
                  System.out.println("+-----------------------------------------------------+");
              break;
            default:
                throw new IllegalArgumentException("use the proper actions");
          };
          //different options are shown depending on the first users input
          if(!betChoise.equals("raise")){
            System.out.println("This is "+p2Name+ "'s hand:");
            System.out.println();
            System.out.println(p2.printHand());
            System.out.println("--------  ---------  ---------  --------");
            System.out.println("| FOLD |  | CHECK |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
            System.out.println("--------  ---------  ---------  --------");
            betChoise = in.nextLine();
              switch (betChoise.toLowerCase()) {
                case "fold":
                System.out.println(p2Name+" Folds, "+p1Name+" Wins");
                    p2.fold();
                  break;
                case "check":
                    p2.check();
                  break;
                case "raise":
                  currentBet += p2.raise(currentBet);
                  System.out.println(currentBet);
                  pot += p2.takeChips(currentBet);
                    System.out.println("The pot is currently holding: " + pot+ " chips");
                    System.out.println("+-----------------------------------------------------+");
                  break;
                case "call":
                    p2.call(currentBet);
                    pot += p2.takeChips(currentBet);
                      System.out.println("The pot is currently holding: " + pot+ " chips");
                      System.out.println("+-----------------------------------------------------+");
                  break;
                default:
                  throw new IllegalArgumentException("use the proper actions");       
            };
          }
          //these options get shown if the first player didn't raise
          else {
            System.out.println("This is "+p2Name+ "'s hand:");
            System.out.println();
            System.out.println(p2.printHand());
            System.out.println("--------  ---------  --------");
            System.out.println("| FOLD |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
            System.out.println("--------  ---------  --------");
            betChoise = in.nextLine();
              switch (betChoise.toLowerCase()) {
                case "fold":
                System.out.println(p2Name+" Folds, "+p1Name+" Wins");
                    p1.fold();
                  break;
                case "raise":
                  currentBet += p2.raise(currentBet);
                  System.out.println(currentBet);
                  pot += p2.takeChips(currentBet);
                    System.out.println("The pot is currently holding: " + pot+ " chips");
                    System.out.println("+-----------------------------------------------------+");
                  break;
                case "call":
                    p1.call(currentBet);
                    pot += p2.takeChips(currentBet);
                      System.out.println("The pot is currently holding: " + pot+ " chips");
                      System.out.println("+-----------------------------------------------------+");
                  break;
                default:
                  throw new IllegalArgumentException("use the proper actions");
              };
            }
            newHand1.insertCard(playingDeck.getRandomCard(), hand1Index);
              hand1Index++;
            newHand2.insertCard(playingDeck.getRandomCard(), hand2Index);
              hand2Index++;
            } catch (Exception e) {
              System.out.println("Please enter (fold, check, raise or call)");
              System.exit(0);
            }          
          }

        //find out the winner of the game
        if (p1.evaluateHand(newHand1) > p2.evaluateHand(newHand2)) {
          System.out.println("Congratulations " + p1Name+ " wins the game");
          p1.addWinnings(pot);
          System.out.println(p1Name+ " Now has: "+ p1.getChips()+ " chips");
          gameOver = true;
        }
        
        else if(p1.evaluateHand(newHand1) ==  p2.evaluateHand(newHand2) ) {
          System.out.println("Its a draw!!!");
          int half1;
          int half2;
          half1 = pot/2;
          half2 = pot;
          p1.addWinnings(half1);
          p2.addWinnings(half2);
          System.out.println(p1Name+ " Now has: "+ p1.getChips()+ " chips");
          System.out.println(p2Name+ " Now has: "+ p2.getChips()+ " chips");
          gameOver = true;
        } 
        else {
          System.out.println("Congratulations " + p2Name+ " wins the game");
          p2.addWinnings(pot);
          System.out.println(p1Name+ " Now has: "+ p2.getChips()+ " chips");
          gameOver = true;
        }
      }
        //this code is not functionnal, since it was low priority
        //when finished this code should clear all decks, hands and bets and restart the game from 0 but the winner keeps their money
        // if (p1.getChips() > 0 && p2.getChips() >0) {
        //   System.out.println("Do you dare play again?");
        //   String playAgain = in.nextLine();
        //   if (playAgain.toLowerCase().charAt(0) == 'y' ) {
        //     playingDeck.clearDeck();
        //     p1.clearHand();
        //     p2.clearHand();
        //     gameAgain=true;
        //   }
        //   else {
        //     gameAgain=false;
        //   }
        // } 
      
      
      }
      
      else {
      System.out.println("+-----------------+");
      System.out.println("Game is postponed");
      System.out.println("+-----------------+");
      }
      in.close();
    //}while(gameAgain);
  }
}
