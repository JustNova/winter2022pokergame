//This class is responisbl for the player objects, all the actions that are related to the
//players can be found here as the player object constructor.

public class Player {
  private String name;
  private int chips;
  private DynamicArray myHand = new DynamicArray();


  //constructoor for the player object, it takes the user input name, a predetermined amount of chips
  //and an empty hand of cards created  with the DynamicArray class
  public Player(String name, int chips, DynamicArray newHand) {
    this.name = name;
    this.chips = chips;
    this.myHand = newHand;
  }
  //lets us see how many chips are in the players "inventory"
  public int getChips() {
    return this.chips;
  }
  //clearing hands
  public void clearHand() {
    this.myHand.removeAll();
  }
  //adding winnings to object
  public void addWinnings(int win) {
    this.chips = this.chips + win;
  }
  //lets us *steal* or take the chips from the player every time they make a bet
  public int takeChips(int currentBet) {
    this.chips = this.chips-currentBet;
    return currentBet;

  }
  //simply lets us get the players name
  public String getName() {
    return this.name;
  }
  //method that uses the DynamicArray printArray method to print the hand
  public String printHand() {
    return this.myHand.printArray();
  }
  //-----Player actions
  //fold exits the program since the game has 2 players, 
  //if there was an array of 3 players this method would have removed the player that folded
  public void fold() {
    
    System.exit(0);
  }
  //does nothing except print the message and skips the turn
  //also it doent show up if the other player raises
  public void check() {
    System.out.println("Turn skipped");
  }
  //raises the bet on the table, this works exponentally since it takes the current bet and adds onto it
  public int raise (int currentBet) {
    return currentBet + 10;
  }
  //matches the current bet 
  public int call (int currentBet) {
    return currentBet;
  }
  //returns true if cards succed in this order (10, Jack, Queen, King, Ace) and all have the same suit
  public boolean royalFlush(DynamicArray hand) {
    sortHand(hand);
    int highestCard = highCard(hand);
    if(highestCard == 14) {
       if (straight(hand) && flush(hand)) {
          return true;
       }
    }
    return false;
  }
  //returns true if there are 5 cards that succed an have the same suit
  public boolean straightFlush(DynamicArray hand) {
    if (straight(hand) && flush(hand)) {
       return true;
    }
    return false;
  } 
  //returns true if there is 4 cards that have the same rank
  public boolean fourOfAKind(DynamicArray hand) {
      int countCard = 0;
      for (int i = 0; i < hand.length(); i++) {
        for (int j = i + 1; j < hand.length(); j++) {
           for (int k = j + 1; k < hand.length(); k++) {
              for (int l = 0; l < hand.length(); l++) {
                  if(hand.getCard(i).getRank().getValue() == hand.getCard(j).getRank().getValue() && hand.getCard(j).getRank().getValue() == hand.getCard(k).getRank().getValue() && hand.getCard(k).getRank().getValue() == hand.getCard(l).getRank().getValue()) {
                     countCard++;
                     if (countCard == 1) {
                     return true;
                  }
            
                  }          
               }
            }     
         }
      }
      return false;
  } 
  //returns true if there is a three of a kind and a pair together
  public boolean fullHouse(DynamicArray hand) {
    sortHand(hand);
    int firstCard = hand.getCard(0).getRank().getValue();
    int lastCard = hand.getCard(hand.length()-1).getRank().getValue();
    int countFirst = 1;
    int countLast = 1;
    for (int i = 1; i < hand.length()-1; i++) {
       if (hand.getCard(i).getRank().getValue() == firstCard) {
          countFirst++;
          System.out.println(countFirst+" count1");
       }
       if (hand.getCard(i).getRank().getValue() == lastCard) {
          countLast++;
          System.out.println(countLast+" count2");
       }
       if (countFirst == 2 && countLast == 3) {
          return true;
       }
       if (countFirst == 3 && countLast == 2) {
          return true;
       }
    }
  return false;
  }
  //returns true if there are 5 cards that have the same suit
  public boolean flush(DynamicArray hand) {
    int hearts = 0;
    int spades = 0;
    int clubs = 0;
    int diamonds = 0;
    for (int i = 0; i < hand.length(); i++) {
      switch (hand.getCard(i).getSuit().toString()) {
        case "Hearts":
          hearts++;
          break;
        case "Spades":
          spades++;
          break;
        case "Clubs":
          clubs++;
          break;
        case "Diamonds":
          diamonds++;
          break;
        default:
        throw new IllegalArgumentException("Invalid card suit:");
      }
      if (hearts == 5  || spades == 5 || clubs == 5 || diamonds == 5) {
       return true;
      }
    }
    return false;
  }  
  //returns true is there are cards that are succeding each other in a row ex.(5,6,7,8,9)
  public boolean straight(DynamicArray hand) {
    sortHand(hand);
    
    int succesions = 1;
    int highestRank = highCard(hand);
    for (int i = hand.length() - 1; i >= 0; i--) {
       if (hand.getCard(i).getRank().getValue() == highestRank-1) {
          highestRank--;
          succesions++;
       }
       if (succesions == 5) {
          return true;
       }
    }
    return false;
  } 
  //returns true if there is 3 cards that have the same rank
  public boolean threeOfAKind(DynamicArray hand) {
      int countCard = 0;
      for (int i = 0; i < hand.length(); i++) {
        for (int j = i + 1; j < hand.length(); j++) {
           for (int k = j + 1; k < hand.length(); k++) {
               if(hand.getCard(i).getRank().getValue() == hand.getCard(j).getRank().getValue() && hand.getCard(j).getRank().getValue() == hand.getCard(k).getRank().getValue()){
                  countCard++;
                  if (countCard == 1) {
                  return true;
                  }          
               }
            }     
         }
      }
      return false;
    } 
  //returns true if there is at least 2 pairs
  public boolean twoPairs(DynamicArray hand) {
    int countPair = 0;
    for (int i = 0; i < hand.length(); i++) {
      for (int j = i + 1; j < hand.length(); j++) {
          if(hand.getCard(i).getRank().getValue() == hand.getCard(j).getRank().getValue()) {
             countPair++;
            if (countPair == 2) {
              return true;
            }
          }          
      }
    }
    return false;
  }
  //returns true if there is at least one pair
  public boolean onePair(DynamicArray hand) {
    int countPair = 0;
    for (int i = 0; i < hand.length(); i++) {
      for (int j = i + 1; j < hand.length(); j++) {
          if(hand.getCard(i).getRank().getValue() == hand.getCard(j).getRank().getValue()) {
             countPair++;
            if (countPair == 1) {
              return true;
            }
          }          
      }
    }
    return false;
  }
  //returns the highest card from the hand
  public int highCard(DynamicArray hand) {
    int maxCardVal = hand.getCard(0).getRank().getValue();
    for (int i = 0; i < hand.length(); i++) {
      if (hand.getCard(i).getRank().getValue() > maxCardVal) {
        maxCardVal = hand.getCard(i).getRank().getValue();
      }
    }
    return maxCardVal;
  }
  
  public static void sortHand(DynamicArray hand) {
    Card temp;
    for (int i = 0; i <hand.length(); i++) {     
      for (int j = i+1; j <hand.length(); j++) {     
        if(hand.getCard(i).getRank().getValue() > hand.getCard(j).getRank().getValue()) {      //swap elements if not in order
             temp = hand.getCard(i);    
             hand.setCard(hand.getCard(j), i);   
             hand.setCard(temp, j); 
        }
      }   
    }
  }
  
  public int evaluateHand(DynamicArray hand) {

    if (royalFlush(hand)) {
        return 10;

    } else if (straightFlush(hand)) {
        return 9;

    } else if (fourOfAKind(hand)) {
        return 8;

    } else if (fullHouse(hand)) {
        return 7;

    } else if (flush(hand)) {
        return 6;

    } else if (straight(hand)) {
        return 5;

    } else if (threeOfAKind(hand)) {
        return 4;

    } else if (twoPairs(hand)) {
        return 3;

    } else if (onePair(hand)) {
        return 2;
    } else {
        return 1;
    }

}  
}
