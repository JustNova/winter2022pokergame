import java.util.Scanner;
public class Pokerold {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    System.out.println("Welcome to Java Poker \nby Vitaliy Aleksyuk");
    Deck playingDeck = new Deck();
    DynamicArray newHand1 = new DynamicArray();
    DynamicArray newHand2 = new DynamicArray();
    int pot = 0;//pot variable, will have the bets deposited into it and then distributed to the winner 
    int hand1Index = 0;
    int hand2Index = 0;
    int currentBet  = 10;
    boolean startGame;
    boolean gameOver = false;
    System.out.println();
    System.out.println("The deck has been created");

    //Creating the players object, using user input for names, 
    //a predetermined value for the chips and a hand object that was initialized earlier
    System.out.println("Player 1, what is your name?");
    String p1Name = in.nextLine();
    //handle value exception
      Player p1 = new Player(p1Name, 1000, newHand1);
      System.out.println("Welcome " + p1Name +" You have: "+ p1.getChips()+" chips");
    System.out.println("Player 2, what is your name?");
    String p2Name = in.nextLine();
    //handle value exception
      Player p2 = new Player(p2Name, 1000, newHand2);
      System.out.println("Welcome " + p2Name +" You have: "+ p2.getChips()+" chips");
    
      // System.out.println("Player 3, what is your name?");
    // String p3Name = in.nextLine();
    //   Player p3 = new Player(p3Name, 1000, newHand); //if i want to try implementing a third player
    //   System.out.println("Welcome " + p3Name);

    //Asking if the players are ready and if they want to play
    System.out.println("Are you ready?\nan entry fee of 10 chips is required\ny or n");
    String ready = in.nextLine();
    //handle value exception
    if (ready.toLowerCase().charAt(0) == 'y' ) {
      startGame=true;
    }
    else {
      startGame=false;
    }
    if (startGame) {
      while (!gameOver){
    //take some chips from each player as entry fee
      System.out.println("10 chips have been taken from each player as entry fee");
      pot += p1.takeChips(currentBet);
      pot += p2.takeChips(currentBet);

    //Filling hands with the 3 initial cards
      for (int i = 0; i < 3; i++) {
        newHand1.insertCard(playingDeck.getRandomCard(), i);
        hand1Index++;
        newHand2.insertCard(playingDeck.getRandomCard(), i);
        hand2Index++;
      }
      //Showing hands for the first time
      String betChoise;
      System.out.println("This is "+p1Name+ "'s hand:");
      System.out.println("Pot: " + pot);
      System.out.println(p1.printHand());
      //showing available option to the player
      System.out.println("--------  ---------  ---------  --------");
      System.out.println("| FOLD |  | CHECK |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
      System.out.println("--------  ---------  ---------  --------");
      betChoise = in.nextLine();
      //checking the user's input and doing the action they input
        switch (betChoise.toLowerCase()) {
          case "fold":
          System.out.println(p1Name+" Folds, "+p2Name+" Wins");
              p1.fold();
            break;
          case "check":
              p1.check();
            break;   
          case "raise":
          currentBet += p1.raise(currentBet);
              System.out.println(currentBet);
              pot += p2.takeChips(currentBet);
              System.out.println("The pot:"+pot);
            break;
          case "call":
              p1.call(currentBet);
              pot += p2.takeChips(currentBet);
              System.out.println("The pot:"+pot);
            break;
          default://throuw illegal arg exception
            break;
        };
        //different options are shown depending on the first users input
        if(!betChoise.equals("raise")){
          System.out.println("This is "+p2Name+ "'s hand:");
          System.out.println();
          System.out.println(p2.printHand());
          System.out.println("--------  ---------  ---------  --------");
          System.out.println("| FOLD |  | CHECK |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
          System.out.println("--------  ---------  ---------  --------");
          betChoise = in.nextLine();
            switch (betChoise.toLowerCase()) {
              case "fold":
              System.out.println(p2Name+" Folds, "+p1Name+" Wins");
                  p2.fold();
                break;
              case "check":
                  p2.check();
                break;
              case "raise":
              currentBet += p2.raise(currentBet);
              System.out.println(currentBet);
              pot += p2.takeChips(currentBet);
              System.out.println("The pot contains: "+pot);
                break;
              case "call":
                  p2.call(currentBet);
                  pot += p2.takeChips(currentBet);
                  System.out.println("The pot contains: "+pot);
                break;
              default://throuw illegal arg exception
                break;
            };
        }
        //these options get shown if the first player didn't raise
        else {
          System.out.println("This is "+p2Name+ "'s hand:");
          System.out.println();
          System.out.println(p2.printHand());
          System.out.println("--------  ---------  --------");
          System.out.println("| FOLD |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
          System.out.println("--------  ---------  --------");
          betChoise = in.nextLine();
            switch (betChoise.toLowerCase()) {
              case "fold":
              System.out.println(p2Name+" Folds, "+p1Name+" Wins");
                  p1.fold();
                break;
              case "raise":
              currentBet += p2.raise(currentBet);
              System.out.println(currentBet);
              pot += p2.takeChips(currentBet);
              System.out.println("The pot contains: "+pot);
                break;
              case "call":
                  p1.call(currentBet);
                  pot += p2.takeChips(currentBet);
                  System.out.println("The pot contains: "+pot);
                break;
              default://throuw illegal arg exception
                break;
            };
          }
            newHand1.insertCard(playingDeck.getRandomCard(), hand1Index);
              hand1Index++;
            newHand2.insertCard(playingDeck.getRandomCard(), hand2Index);
              hand2Index++;
            System.out.println("Round 2 has begun");
            System.out.println("The pot currently has: "+pot+" chips");
            System.out.println("A card has been added to each player's hand");
            System.out.println("This is "+p2Name+ "'s hand:");
      System.out.println(p2.printHand());
      //showing available option to the player
      System.out.println("--------  ---------  ---------  --------");
      System.out.println("| FOLD |  | CHECK |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
      System.out.println("--------  ---------  ---------  --------");
      betChoise = in.nextLine();
      //checking the user's input and doing the action they input
        switch (betChoise.toLowerCase()) {
          case "fold":
          System.out.println(p2Name+" Folds, "+p1Name+" Wins");
              p2.fold();
            break;
          case "check":
              p2.check();
            break;   
          case "raise":
          currentBet += p2.raise(currentBet);
              System.out.println(currentBet);
              pot += p2.takeChips(currentBet);
              System.out.println("The pot:"+pot);
            break;
          case "call":
              p1.call(currentBet);
              pot += p2.takeChips(currentBet);
              System.out.println("The pot:"+pot);
            break;
          default://throuw illegal arg exception
            break;
        };
        //different options are shown depending on the first users input
        if(!betChoise.equals("raise")){
          System.out.println("This is "+p1Name+ "'s hand:");
          System.out.println();
          System.out.println(p1.printHand());
          System.out.println("--------  ---------  ---------  --------");
          System.out.println("| FOLD |  | CHECK |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
          System.out.println("--------  ---------  ---------  --------");
          betChoise = in.nextLine();
            switch (betChoise.toLowerCase()) {
              case "fold":
              System.out.println(p1Name+" Folds, "+p2Name+" Wins");
                  p1.fold();
                break;
              case "check":
                  p1.check();
                break;
              case "raise":
              currentBet += p1.raise(currentBet);
              System.out.println(currentBet);
              pot += p1.takeChips(currentBet);
              System.out.println("The pot contains: "+pot);
                break;
              case "call":
                  p1.call(currentBet);
                  pot += p1.takeChips(currentBet);
                  System.out.println("The pot contains: "+pot);
                break;
              default://throuw illegal arg exception
                break;
            };
        }
        //these options get shown if the first player didn't raise
        else {
          System.out.println("This is "+p1Name+ "'s hand:");
          System.out.println();
          System.out.println(p1.printHand());
          System.out.println("--------  ---------  --------");
          System.out.println("| FOLD |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
          System.out.println("--------  ---------  --------");
          betChoise = in.nextLine();
            switch (betChoise.toLowerCase()) {
              case "fold":
              System.out.println(p1Name+" Folds, "+p2Name+" Wins");
                  p1.fold();
                break;
              case "raise":
              currentBet += p1.raise(currentBet);
              System.out.println(currentBet);
              pot += p1.takeChips(currentBet);
              System.out.println("The pot contains: "+pot);
                break;
              case "call":
                  p1.call(currentBet);
                  pot += p1.takeChips(currentBet);
                  System.out.println("The pot contains: "+pot);
                break;
              default://throuw illegal arg exception
                break;
            };
        }
        newHand1.insertCard(playingDeck.getRandomCard(), hand1Index);
          hand1Index++;
        newHand2.insertCard(playingDeck.getRandomCard(), hand2Index);
          hand2Index++;
          System.out.println("The last round has begun");
          System.out.println("The pot currently has: "+pot+" chips");
          System.out.println("A last card has been added to each player's hand");
          System.out.println("This is "+p1Name+ "'s hand:");
          System.out.println("Pot: " + pot);
          System.out.println(p1.printHand());
          //showing available option to the player
          System.out.println("--------  ---------  ---------  --------");
          System.out.println("| FOLD |  | CHECK |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
          System.out.println("--------  ---------  ---------  --------");
          betChoise = in.nextLine();
          //checking the user's input and doing the action they input
            switch (betChoise.toLowerCase()) {
              case "fold":
              System.out.println(p1Name+" Folds, "+p2Name+" Wins");
                  p1.fold();
                break;
              case "check":
                  p1.check();
                break;   
              case "raise":
              currentBet += p1.raise(currentBet);
                  System.out.println(currentBet);
                  pot += p2.takeChips(currentBet);
                  System.out.println("The pot:"+pot);
                break;
              case "call":
                  p1.call(currentBet);
                  pot += p2.takeChips(currentBet);
                  System.out.println("The pot:"+pot);
                break;
              default://throuw illegal arg exception
                break;
            };
            //different options are shown depending on the first users input
            if(!betChoise.equals("raise")){
              System.out.println("This is "+p2Name+ "'s hand:");
              System.out.println();
              System.out.println(p2.printHand());
              System.out.println("--------  ---------  ---------  --------");
              System.out.println("| FOLD |  | CHECK |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
              System.out.println("--------  ---------  ---------  --------");
              betChoise = in.nextLine();
                switch (betChoise.toLowerCase()) {
                  case "fold":
                  System.out.println(p2Name+" Folds, "+p1Name+" Wins");
                      p2.fold();
                    break;
                  case "check":
                      p2.check();
                    break;
                  case "raise":
                  currentBet += p2.raise(currentBet);
                  System.out.println(currentBet);
                  pot += p2.takeChips(currentBet);
                  System.out.println("The pot contains: "+pot);
                    break;
                  case "call":
                      p2.call(currentBet);
                      pot += p2.takeChips(currentBet);
                      System.out.println("The pot contains: "+pot);
                    break;
                  default://throuw illegal arg exception
                    break;
                };
            }
            //these options get shown if the first player didn't raise
              else {
                System.out.println("This is "+p2Name+ "'s hand:");
                System.out.println();
                System.out.println(p2.printHand());
                System.out.println("--------  ---------  --------");
                System.out.println("| FOLD |  | RAISE |  | CALL |" + " The current minimum bet is: "+ currentBet);
                System.out.println("--------  ---------  --------");
                betChoise = in.nextLine();
                  switch (betChoise.toLowerCase()) {
                    case "fold":
                    System.out.println(p2Name+" Folds, "+p1Name+" Wins");
                        p1.fold();
                      break;
                    case "raise":
                    currentBet += p2.raise(currentBet);
                    System.out.println(currentBet);
                    pot += p2.takeChips(currentBet);
                    System.out.println("The pot contains: "+pot);
                      break;
                    case "call":
                        p1.call(currentBet);
                        pot += p2.takeChips(currentBet);
                        System.out.println("The pot contains: "+pot);
                      break;
                    default://throuw illegal arg exception
                      break;
                  };
              }
        gameOver = true;
    }
    }
    else {
    System.out.println("Game is postponed");
    }
  }
}
