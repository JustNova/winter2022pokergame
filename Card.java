//Card class uses the enums to created a sigle card object
public class Card {
    
    private Suit suit;
    private Rank rank;
    //constructor for the class, take a suit and rank enum as inputs to create a card object
    public Card(Suit suit, Rank rank) {
        this.suit = suit;
        this.rank = rank;
    }   
    //method that returns the suit of the card, 
    //used in cnjunction with the enum methods to get a string value back
    public Suit getSuit() {
        return this.suit;
    }
    //method that returns the rank of the card
    //used in cnjunction with the enum methods to get an int value back
    public Rank getRank() {
        return this.rank;
    }
    //prints the card as a string
    public String toString() {
        String str = ""; 
        str += this.rank + " of " + this.suit.toString();
        return str;
    }
}