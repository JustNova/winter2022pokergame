//This class is responsible for the creation and actions related to the deck of cards
//that is used througout the game
import java.util.Random;
public class Deck {
  private DynamicArray cards = new DynamicArray();
  
  //constructor class that uses a for each loop to create cards of every suit and rank using the enums
  public Deck(){
    for ( Suit suit : Suit.values()) {
      for (Rank rank : Rank.values()) {
        this.cards.addCard(suit, rank);
      }
    }
  }
  public void clearDeck() {
    this.cards.removeAll();
  }
  //Method to print the deck, uses the printArray method from the DynamicArray class, mostly used for debugging purpuses but can also be implemented in game
  public String printDeck() {
    return this.cards.printArray();
  }
  //method to get the size of the deck, also mostly used for debugging puroses
  public int getLength() {
    return this.cards.length();
  }
  //Method that gets a random card from the deck, this is an alernatie to a shuffle method
  //since its using a random number generator with dynamic bounds and keeps a temprary card taht is removed by the remove method
  public Card getRandomCard() {
  Random rand = new Random();
  int randomCard = rand.nextInt(this.cards.length()-1);
  //System.out.println(randomCard + 1); //testing purposes  
  Card tempCard;
  tempCard = cards.getCard(randomCard);
  //System.out.println(tempCard.getRank().toString() + " of " + tempCard.getSuit());  //This is used for testing purposes 
  this.cards.remove(randomCard);
  return tempCard;
  }
}
