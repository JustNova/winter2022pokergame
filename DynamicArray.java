//Class responsible for the creation and modification of the dynamic arrays used troughout the game
//Contains helper methods, getters, setters and mutators useful for modifying the dynamic arrays
public class DynamicArray {

    private Card[] dynamicArray;
    private int pos;
  
    public DynamicArray(){
      this.dynamicArray = new Card[100];
      this.pos = 0;
    }
    //method that adds cards using the Card class and the enums as input for the Card constructor
    public void addCard(Suit suit, Rank rank) {
      this.dynamicArray[this.pos] = new Card(suit, rank) ;
      this.pos++;
      //throw exception is more cards than 52 are being added
    }
    //method to print the dynamic array without the empty space being shown
    public String printArray() {
     String arrayString = "";
      for (int i = 0; i < dynamicArray.length; i++) {
        if(dynamicArray[i] != null) {

          arrayString = arrayString + this.dynamicArray[i] + "\n";
          
        }
        
      }
      return arrayString;
    }
    //keeps track of the lenght of each dynamic array object that we create
    public int length(){
      return pos;
    }
    //method that return a boolean value if a card is contained in the dynamic array or not
    public boolean containsCard(Card card){
      for (int i = 0; i < this.pos; i++) {
        if(this.dynamicArray[i].equals(card)){
          return true;
        }
      }
      return false;
    }
    //returns the Card object based on the index input
    public Card getCard(int i){
      return this.dynamicArray[i];
    }
    //takes a card and an index as input to then change the current card to the new one that you want.
    public void setCard(Card card, int i) {
      this.dynamicArray[i] = card;
      }
    //This method removed a desired card from the dynamic array
    //Used in conjunction with the getRandomCard metod in the Deck class where the removed card get stored as a temp object
    public void remove(int i) {
      for (int j = i; j < this.pos; j++) {
      //careful about the order!
      this.dynamicArray[j] = this.dynamicArray[j + 1];
      }
      this.dynamicArray[this.pos - 1] = null;
      this.pos--;
      }
      public void removeAll() {
        for (int i = 0; i < dynamicArray.length; i++) {
          this.dynamicArray[i] = null;
          this.pos = 0;
        }
      }
    //This method takes a card and an index as an input and then places the card in that place
    //while moving the other cards one spot further
    public void insertCard(Card card, int index) {
      for (int j = this.pos; j >= index + 1; j--) {
      this.dynamicArray[j] = this.dynamicArray[j-1];
      }
      this.dynamicArray[index] = card;
      this.pos++;
      }
      
      
  }
  