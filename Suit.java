//suit enum, used to get Strings from the enums and to set the card suits 
public enum Suit {
  HEARTS ("Hearts"),
  SPADES ("Spades"),
  DIAMONDS ("Diamonds"),
  CLUBS ("Clubs"); 

    private final String text;
  
    private Suit(final String text) {
        this.text = text;
    }


    public String toString() {
        return text;
    }
}
